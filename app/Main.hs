module Main where

import Lib
import System.Environment
import System.Exit
import System.Random
import System.Random.Shuffle
import Data.Typeable
import Data.Maybe
import Text.Read
import Text.Printf
import Control.Monad

type Error = String
data Tile = Tile {isFlagged:: Bool
		 ,isMine:: Bool
		 ,isDigged:: Bool
		 ,minesArround:: Integer} deriving Show


isInRange:: Integer -> Integer -> Integer -> Bool
isInRange x min max = (x >= min) && (x <= max)

validateArgs:: [String] -> (Maybe Error, Maybe(Integer, Integer, Integer))
validateArgs xs 
    | length xs < 3 = (Just "Too few arguments", Nothing)
    | length xs > 3 = (Just "Too many arguments", Nothing)
    | length parsed /= length xs = (Just "Wrong type of paramters", Nothing)
    | mines > width * height = (Just "Too many mines", Nothing)
    | otherwise = case isInRangeOf width height mines of
			True -> (Nothing, Just (width, height, mines))
		  	False -> (Just "width and height are both integer numbers in the range 10 - 20 \n mine-count is an integer number in the range 4 - 199", Nothing)
        where
	   parsed = [k | k <- fmap readMaybe xs::[Maybe Integer], k /= Nothing]
           (Just width) = parsed !! 0
           (Just height) = parsed !! 1
           (Just mines) = parsed !! 2
	   isInRangeOf w h m = (isInRange w 10 20) && (isInRange h 10 20) && (isInRange m 4 199)
        --where (w : h : m) = xs

generateMinedBoard:: Integer -> Integer -> Integer -> [Tile]
generateMinedBoard width height mines = shuffle' (map (Tile False True False) [1..mines] ++ map (Tile False False False) [(mines + 1)..width * height]) (fromIntegral (height * width)) (mkStdGen 120)

getXYByTile:: Integer -> Integer -> (Integer, Integer)
getXYByTile w x = (x `div` w, x `mod` w)

getTileByXY:: [Tile] -> Integer -> Integer -> (Integer, Integer) -> Maybe Tile
getTileByXY state w h (x, y)
	| x < 0 = Nothing 
	| y < 0 = Nothing
	| y >= w = Nothing
	| x >= h = Nothing
	| otherwise = Just $ state !! (fromIntegral (x * w + y)) 

getIsMine::Tile -> Bool
getIsMine Tile {isMine = x} = x

unwrapMaybeTile:: Maybe Tile -> Integer
unwrapMaybeTile tile = case tile of
	Nothing -> 0
	Just tile' -> if getIsMine tile' then 1 else 0
	
setSurroundingMinesOfTile:: Tile -> Integer -> Tile
setSurroundingMinesOfTile (Tile x y z _) n = Tile x y z n

setupGame:: Integer -> Integer -> Integer -> ([Tile], Integer)
setupGame width height mines = (zipWith calculateSurroundingMines [0..] board0, width)
	where
		board0 = generateMinedBoard width height mines
		functions = [succ, pred, id]:: [(Integer->Integer)]
		fs = [(f, g) | g <- functions, f <- functions]
		calculateSurroundingMines index tile = setSurroundingMinesOfTile tile (sum $ fmap unwrapMaybeTile (fmap (getTileByXY board0 width height) (fmap (\(f,g) -> (f x, g y)) fs)))
			where (x, y) = getXYByTile width index

printBoard:: ([Tile], Integer) -> IO()
printBoard (board, width) = putStr $ printHeader ++ unwords (zipWith formatTile [0..] board)
	where
		printTile (Tile flag mine digged around) 
			| flag = "[ ! ]"
			| digged == False = "[   ]"
			| mine = "[ * ]"
			| otherwise = "[ " ++ show around ++ " ]"
		formatTile index tile
			| index `mod` width == 0 = printf "\n%03d %s" (index `div` width) (printTile tile)  --"\n" ++ printTile tile
			| otherwise = printTile tile
		printHeader = "    " ++ (unwords (fmap formatHeader [0..width-1]))
		formatHeader x = printf " %03d " (x `div` 1)

parseAction:: [Char] -> Integer -> Integer -> IO ([Char], Integer, Integer)
parseAction err width height = do 
	when (err /= "") $ putStrLn err
	action <- getLine
	let (err, parsed) = parseAction' (words action)
	if parsed == Nothing then parseAction (fromJust err) width height else return (fromJust parsed)
	where
		parseAction' xs
			| length xs < 3 = (Just "Too few arguments", Nothing)
			| action' == True = (Just "Bad action", Nothing)
			| x' == Nothing = (Just "x coord should be a number", Nothing)
			| y' == Nothing = (Just "x coord should be a number", Nothing)
			| xisinrange == False = (Just "x value is not in range", Nothing)
			| yisinrange == False = (Just "y value is not in range", Nothing)
			| otherwise = (Nothing, Just (action, x'', y''))
			where 
				(action : x : y : []) = xs 
				action' = action /= "Dig" && action /= "Flag"
				x' = (readMaybe x)::(Maybe Integer)
				y' = (readMaybe y)::(Maybe Integer)
				(Just x'') = x'
				(Just y'') = y'
				xisinrange = isInRange x'' 0 height
				yisinrange = isInRange y'' 0 width

loseGame:: [Tile] -> Integer -> IO ()
loseGame board width = do
	putStr "\ESC[2J\ESC[H\n"
	printBoard (board, width)
	putStr "\nYou lost the game!\n"
	die ""

winGame:: IO ()
winGame = do
	putStr "You win the game\n"
	die ""

progressGame:: [Tile] -> Integer -> Integer -> Integer -> IO()
progressGame board width height mines = do
	putStr "\ESC[2J\ESC[H\n"
	when (win) $ winGame
	printBoard (board, width)
	putStr "\nWhat is your next move?\n"
	action <- parseAction "" width height
	when (loose action) $ loseGame exposemine width 
	--print action
	progressGame (next board action) width height mines
	where
		win = mines == sum (fmap convertMine board)
			where
				convertMine (Tile isflag ismined isdigged minesaround)
					| isdigged = 0
					| otherwise = 1
		loose (act, x, y) = act == "Dig" && ismine (fromJust (getTileByXY board width height (x, y)))
				where
					ismine (Tile _ ismined _ _ ) = ismined
		exposemine = fmap exposeifmine board
				where
					exposeifmine (Tile x ismined isdigged y)
						| ismined = (Tile x ismined (not isdigged) y )
						| otherwise = (Tile x ismined isdigged y)
		next board (action, x, y)
			| action == "Flag" = zipWith flag [0..] board
			| action == "Dig" = open' board [(x, y)]
			| otherwise = undefined
			where
				flag index (Tile isflag ismined isdigged minesaround)
					| index == width * x + y = (Tile (not isflag) ismined isdigged minesaround)
					| otherwise = (Tile isflag ismined isdigged minesaround)
				open' mboard openstack  
					| openstack == [] = mboard
					| otherwise = open' mboard' (tail openstack ++ openstack'')
					where
						(x', y') = head openstack
						(openstack', mboard') = unzip (zipWith (open x' y') [0..] mboard) -- ::[([(Integer, Integer)], Tile)]
						openstack'' = concat openstack'
				open x' y' index (Tile isflag ismined isdigged minesaround)
					| index /= width * x' + y' = ([], (Tile isflag ismined isdigged minesaround))
					| isflag == True = ([], (Tile isflag ismined isdigged minesaround))
					| isdigged == True = ([], (Tile isflag ismined isdigged minesaround))
					| flagneighbors >= minesaround = (filtered_coord, (Tile isflag ismined True minesaround))
					| otherwise = ([], (Tile isflag ismined True minesaround))
					where
						functions = [succ, pred, id]:: [(Integer->Integer)]
						fs = [(f, g) | g <- functions, f <- functions]
						neighbors_coord = fmap (\(f,g) -> (f x', g y')) fs
						filtered_coord = filter checkxy neighbors_coord
						checkxy (x, y) = (isInRange x 0 (height + (-1))) && (isInRange y 0 (width + (-1)))
						maybe_neighbors = (fmap (getTileByXY board width height) neighbors_coord)::[Maybe Tile]
						neighbors = [n | (Just n) <- maybe_neighbors]
						flagneighbors = sum $ fmap convertTileFlag neighbors
						convertTileFlag (Tile isflag ismined isdigged minesaround)
							| isflag == True = 1
							| otherwise = 0



startGame :: Maybe(Integer, Integer, Integer) -> IO ()
startGame (Just (width, height, mines)) = do
	let (state0, _) = (setupGame width height mines)
	progressGame state0 width height mines
	

startGame Nothing = undefined --unreachable code

main :: IO ()
main = do
    args <- getArgs
    let (err, params) = validateArgs args
    case err of
       Just msg -> die msg
       Nothing -> startGame(params)
                      
