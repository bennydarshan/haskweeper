{-# OPTIONS -Wall #-}
-- Noam Waissman
-- 206199515
-- Binyamin Darshan
-- 203249131

foo:: Maybe Int -> Int
foo x
   | Nothing = 0
   | (Just y) = y
